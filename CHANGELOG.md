# Changelog

All notable changes to `laravel-environments` will be documented in this file.

## 1.0.0 - 2022-03-31

- Initial release
