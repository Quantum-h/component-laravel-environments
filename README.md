# Add environments and environmentable behaviour to a Laravel

## Credits

- [Carlos Rodriguez]
- [David Vazquez]

## Installation

You can install the package via composer:

``` bash
composer require quamtumh/laravel-environments
```

The package will automatically register itself.

You can publish the migration with:
```bash
php artisan vendor:publish --provider="Quantumh\Environments\EnvironmentsServiceProvide" --tag="tags-migrations"
```

After the migration has been published you can create the `environments` and `environmentables` tables by running the migrations:

```bash
php artisan migrate
```

You can optionally publish the config file with:
```bash
php artisan vendor:publish --provider="Quantumh\Environments\EnvironmentsServiceProvide" --tag="tags-config"
```


## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information
