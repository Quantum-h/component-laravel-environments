<?php

return [

    /*
     * The fully qualified class name of the environment model.
     */
    'environment_model' => Quantumh\Environments\Environment::class,
];
