<?php

namespace Quantumh\Environments;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;


class Environment extends BaseModel
{
    use HasFactory;

    const PERMISSION_SUFFIX = 'environments';

    public $guarded = [];

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            $user = auth()->user();
            if($user) {
                $user->environments()->attach($model->id);
            }
        });

        static::addGlobalScope(new EnvironmentScope);
    }
}
