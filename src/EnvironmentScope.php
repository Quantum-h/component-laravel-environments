<?php

namespace Quantumh\Environments;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class EnvironmentScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if ($model instanceof User) return;

        $user = auth()->guard('web')->user();

        if ($user) {
            //We are using a custom method instead of relation $user->environments to avoid infinity loop (because environments is checking itself)
            if ($model->getTable() == Environment::PERMISSION_SUFFIX) {
                $environments = $this->getUserEnvironments($user->id);
                if (count($environments)) {
                    $builder->whereIntegerInRaw('environments.id', $environments);
                }
            } else {
                $environmentId = session()->has('environment_id') ? session()->get('environment_id') : $this->getUserEnvironments($user->id)->first();

                $builder
                    ->whereHas('environments', function (Builder $query) use ($environmentId) {
                        $query->where('environments.id', $environmentId);
                    });
            }
        } else {
            if ($model->getTable() != Environment::PERMISSION_SUFFIX) {
                $environmentId = session()->has('environment_id') ? session()->get('environment_id') : Environment::first()->id;
                $builder
                    ->whereHas('environments', function (Builder $query) use ($environmentId) {
                        $query->where('environments.id', $environmentId);
                    });
            }
        }
    }

    public function getUserEnvironments($id)
    {
        return DB::table('envirables')->where('envirable_id', $id)->where('envirable_type', 'App\Models\User')->get()->pluck('environment_id');
    }
}

