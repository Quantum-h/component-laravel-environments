<?php

namespace Quantumh\Environments;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class EnvironmentsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('laravel-environments')
            ->hasConfigFile()
            ->hasMigration('create_environment_tables');
    }
}
