<?php

namespace Quantumh\Environments;

use ArrayAccess;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


trait HasEnvironments
{
    protected array $queuedEnvironments = [];

    public static function getEnvironmentClassName(): string
    {
        return config('environments.environment_model', Environment::class);
    }

    public static function bootHasEnvironments()
    {
        static::addGlobalScope(new EnvironmentScope);

        static::created(function (Model $environmentableModel) {
            if(session()->has('environment_id')) {
                $currentEnvironmentId = DB::table('environments')->where('id', session()->get('environment_id'))->first()->id;
            }else{
                $currentEnvironmentId = DB::table('environments')->first()->id;
            }
            $environmentableModel->syncEnvironments([$currentEnvironmentId]);
        });

    }

    public function environments(): MorphToMany
    {
        return $this
            ->morphToMany(self::getEnvironmentClassName(), 'envirable');
    }

    public function syncEnvironments(array|ArrayAccess $environments): static
    {
        $className = static::getEnvironmentClassName();

        $environments = collect($className::find($environments));

        $this->environments()->sync($environments->pluck('id')->toArray());

        return $this;
    }
}
