<?php

namespace Quantumh\Environments;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserEnvironmentScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if(session()->has('environment_id')) {
            if ($model->getTable() == User::PERMISSION_SUFFIX) {
                $builder->with('actualEnvironment')->whereHas('actualEnvironment');
            }
        }
    }
}
